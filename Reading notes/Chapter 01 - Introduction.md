# Chapter 1: Introduction

Rachel's summary notes

## Vocabulary

* [Operating System](https://en.wikipedia.org/wiki/Operating_system)
* [Hardware](https://en.wikipedia.org/wiki/Computer_hardware)
* [CPU](https://en.wikipedia.org/wiki/Central_processing_unit)
* [Memory](https://en.wikipedia.org/wiki/Computer_memory)
* [Input/output devices](https://en.wikipedia.org/wiki/Input/output)
* [Application programs](https://en.wikipedia.org/wiki/Application_software)
* Control program - such as the OS, a program which manages program execution, prevents errors and improper use.
* [Moore's law](https://en.wikipedia.org/wiki/Moore%27s_law)
* [Kernel](https://en.wikipedia.org/wiki/Kernel_(operating_system))
* System programs
* [Middleware](https://en.wikipedia.org/wiki/Middleware)
* [Bootstrap program](https://en.wikipedia.org/wiki/Booting)
* [ROM](https://en.wikipedia.org/wiki/Read-only_memory)
* EEPROM
* [Firmware](https://en.wikipedia.org/wiki/Firmware)
* [System processes/daemons](https://en.wikipedia.org/wiki/Daemon_(computing))
* [Software/hardware interrupt](https://en.wikipedia.org/wiki/Interrupt)
* [System call/monitor call](https://en.wikipedia.org/wiki/System_call)
* Interrupt vector
* [RAM](https://en.wikipedia.org/wiki/Random-access_memory)
* DRAM
* [von Neumann architecture](https://en.wikipedia.org/wiki/Von_Neumann_architecture)
* [Instruction register](https://en.wikipedia.org/wiki/Instruction_register)
* [Direct memory access (DMA)](https://en.wikipedia.org/wiki/Direct_memory_access)
* Fault tolerant
* [Asymmetric multiprocessing](https://en.wikipedia.org/wiki/Asymmetric_multiprocessing), [symmetric multiprocessing](https://en.wikipedia.org/wiki/Symmetric_multiprocessing)
* Asymmetric, symmetric clustering
* [Parallelization](https://en.wikipedia.org/wiki/Parallel_computing)
* Distributed lock manager (DLM)
* Storage-area networks (SANs)

---

# Notes
