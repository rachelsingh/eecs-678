# Chapter 2: Operating system structures

Rachel's summary notes

## Vocabulary

* User interface, command line interface (CLI), graphical user interface (GUI)
* Batch interface
* Shared memory
* Message passing
* Command interpreter
* Shells - Bourne, C, Bourne-Again, Korn
* Desktop, icons, folders
* Gestures (mobile)
* System administrators
* Power users
* Shell scripts
* System calls
* Application programming interface (API)
* libc
* System-call interface
* Pushing and popping off the stack
* Types of system calls
	* Process control
	* File manipulation
	* Device manipulation
	* Information maintenance
	* Communications
	* Protection
* Debugger
* Lock shared data
* Single step (CPU mode)
* Daemons/services/subsystems
* Client, server
* System programs/system utilities
* Registry
* User goals, system goals
* Policy, mechanism
* Emulators
* OS structures
	* Monolithic
	* Simple
	* Layered
	* Microkernel
	* Hybrid
* Loadable kernel modules
* Performance tuning
* Bottlenecks
* Log file, core dump, crash dump
* DTrace
* Profiling
* Providers and consumers of probes
* Enabling control blocks (ECBs)
* System generation SYSGEN
* Booting
* Bootstrap program/bootstrap loader
* Read-only memory (ROM)
* Erasable programmable read-only memory (EPROM)
* Firmware
* Boot block
* GRUB
* Boot disk/system disk

---

# Notes
