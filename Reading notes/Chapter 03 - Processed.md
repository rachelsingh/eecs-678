# Chapter 3: Processes

Rachel's summary notes

## Vocabulary

* Process
* User programs / tasks
* Text section
* Program counter
* Process stack
* Data section
* Process heap
* Process state - New, running, waiting, ready, terminated
* Process control block (PCB)
* Task control block
* Thread
* Parent process
* Child process
* Sibling process
* Process scheduler
* Job queue
* Ready queue
* Device queue
* Long-term scheduler / job scheduler
* Short-term scheduler / CPU scheduler
* Medium-term scheduler
* I/O-bound process
* CPU-bound process
* Swapping
* State save, state restore
* Context switching
* Foreground application, background application
* Tree of processes
* Process identifier (pid)
* Cascading termination
* Zombie process
* Orphan process
* Interprocess communication (IPC)
* Shared memory
* Message passing
* Producer process, consumer process
* Unbounded buffer, bounded buffer
* Direct communication
* Blocking, nonblocking
* Synchronous, asynchronous
* Advanced local procedure call (ALPC)
* Connection ports
* Communication ports
* Section object
* Socket
* Connection-oriented sockets (TCP)
* Connectionless sockets (UDP)
* Loopback
* Port
* Stub
* Microsoft Interface Definition Language (MIDL)
* big-endian, little-endian
* External data representation (XDR)
* Matchmaker
* Pipe
* Write-end, read-end
* Anonymous pipes
* Named pipes
 

---

# Notes

