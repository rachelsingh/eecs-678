# Chapter 5: Process synchronization

Rachel's summary notes

## Vocabulary

* Cooperating process
* Race condition
* Process synchronization and coordination
* The critical-section problem
	* Critical section
	* Entry section
	* Exit section
	* Remainder section
	* Preemptive kernels, non-preemptive kernels
* Peterson's solution
* Synchronization hardware
	* Locking
	* Atomically
* Mutex locks
	* Mutex stands for "Mutual exclusion"
	* Busy waiting
	* Spinlock
* Semaphores
	* Counting semaphore
	* Binary semaphore
* Deadlocks
* Starvaction/Indefinite blocking
* Priority inversion
* Priority-inheritance protocol
* Classic synchronization problems
	* The bounded-buffer problem
	* The readers-writers problem
	* The dining-philosophers problem
* Monitors
* Abstract data type (ADT)
* Conditional-wait construct
* Priority number
* Synchronization in Windows
	* Dispatcher objects
	* Events
	* Signaled state, nonsignaled state
	* Critical-section object
* Synchronization in Linux
* Synchronization in Solaris
	* Adaptive mutex
	* Turnstile
	* Priority-inheritance protocol
* Pthreads synchronization
	* Named and unnamed semaphores
* Alternatives
	* Transactional memory, Software transactional memory (STM), Hardware transactional memory (HTM)
	* OpenMP
	* Functional programming languages

---

# Notes

